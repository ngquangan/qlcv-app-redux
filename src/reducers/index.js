import {combineReducers} from 'redux';
import tasks from './tasks';
import displayForm from './displayForm';
import taskEditing from './taskEditing';
import filterTask from './filterTask';
import searchTask from './searchTask';
import sortTask from './sortTask';
const myReducer = combineReducers({
    tasks,
    displayForm,
    taskEditing,
    filterTask,
    searchTask,
    sortTask
});

export default myReducer;