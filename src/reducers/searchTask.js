import * as types from '../constants/actionTypes'
let initState = '';

let myReducer = (state = initState, action) => {
    switch(action.type){
        case types.SEARCH_TASK:{
            console.log(action);
            return action.searchName;
        }

        default: return state;
    }
}

export default myReducer;