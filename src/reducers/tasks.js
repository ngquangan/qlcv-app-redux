import * as types from '../constants/actionTypes'
let data = JSON.parse(localStorage.getItem('tasks'));
let initState = data ? data:[];

let myReducer = (state = initState, action) => {
    let randomstring = require('randomstring');
    switch(action.type){
        case types.LIST_ALL:{
            return state;
        }
        case types.ADD_TASK:{ //xu ly cai hoat dong ma nguoi dung gui len
            if(action.task.id === ''){
                let id = randomstring.generate();
                let newTask = {
                    id: id,
                    name: action.task.name,
                    status: action.task.status
                }
                state.push(newTask);
            }else{
                let pos = findIndex(state, action.task.id);
                state.splice(pos,1,action.task);
            }
            localStorage.setItem('tasks',JSON.stringify(state));
            return [...state];
        }
        case types.TOGGLE_STATUS:{
            let pos = findIndex(state, action.id);
            
            state[pos] = {
                ...state[pos],
                status : !state[pos].status
            }

            localStorage.setItem('tasks', JSON.stringify(state));
            return [...state];
        }
        case types.DELETE_TASK:{
            state = state.filter((task) => {
                return task.id !== action.id;
            });
            localStorage.setItem('tasks', JSON.stringify(state));
            return [...state];
        }
        default: return state;
    }
}
function findIndex(tasks, id){
    let index = -1;
    for(let i = 0; i < tasks.length ;i++){
        if(tasks[i].id === id){
            index = i;
        }
    }
    return index;
}
export default myReducer;