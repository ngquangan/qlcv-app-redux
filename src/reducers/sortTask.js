import * as types from '../constants/actionTypes'
let initState = {
    by: 'name',
    value: 1
};

let myReducer = (state = initState, action) => {
    switch(action.type){
        case types.SORT_TASK:{
            return action.sort;
        }

        default: return state;
    }
}

export default myReducer;