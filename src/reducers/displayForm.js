import * as types from '../constants/actionTypes'
let initState = false;

let myReducer = (state = initState, action) => {
    switch(action.type){
        case types.TOGGLE_FORM:{
            let status = !state;
            return status;
        }
        case types.CLOSE_FORM:{
            return false;
        }
        case types.OPEN_FORM:{
            return true;
        }
        default: return state;
    }
}

export default myReducer;