import * as types from '../constants/actionTypes'
let initState = {
    name : '',
    status: -1
};

let myReducer = (state = initState, action) => {
    switch(action.type){
        case types.FILTER_TASK:{
            let filter = action.filter;
            return filter;
        }

        default: return state;
    }
}

export default myReducer;