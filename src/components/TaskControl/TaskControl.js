import React, { Component } from 'react';
import TaskSearchControl from '../TaskSearchControl/TaskSearchControl';
import TaskSortControl from '../TaskSortControl/TaskSortControl';

class TaskControl extends Component {

    render() {
        return (
            <div className="row">
                <TaskSearchControl />
                <TaskSortControl />
            </div>
        );
    }
}

export default TaskControl;