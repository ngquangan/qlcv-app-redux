import React, { Component } from 'react';
import TaskControl from '../TaskControl/TaskControl';
import TaskList from '../TaskList/TaskList';
import TaskForm from '../TaskForm/TaskForm';
import {connect} from 'react-redux';
import * as actions from '../../actions/index'
class App extends Component {

    onToggleForm = () => {
        if(this.props.taskEditing.id !== ''){
            this.props.onOpenForm();
        }else{
            this.props.onToggleForm();
        }
        this.props.onAddTask({
            id : '',
            name: '',
            status: false
        });
    }

    render() {  
        let isForm = this.props.displayForm;
        let isDisplayForm = isForm ? <TaskForm 
            /> : '';
        return (
            <div className="AppName">
                <div className="container">
                    <div className="header text-center mt-3">
                        <h2>Quản Lý Công Việc</h2>
                        <hr />
                    </div>
                    <div className="content">
                        <div className="row">
                            <div className="col-sm-4">
                                {isDisplayForm}
                            </div>
                            <div className={isForm ? "col-sm-8" : "col-sm-12"}>
                                <button className="btn btn-primary mb-3" onClick = {() => this.onToggleForm()}>Thêm Công Việc</button>
                                <TaskControl
                                />
                                <TaskList 
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        displayForm: state.displayForm,
        taskEditing: state.taskEditing
    }
}
const mapDispatchToProps = (dispatch,props) => {
    return {
        onToggleForm : () => {
            dispatch(actions.toggle_form());
        },
        onOpenForm : () => {
            dispatch(actions.open_form());
        },
        onAddTask: (task) => {
            dispatch(actions.edit_task(task));
        }
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(App);