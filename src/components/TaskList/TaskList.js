import React, { Component } from 'react';
import TaskListItem from '../TaskListItem/TaskListItem';
import {connect} from 'react-redux';//ket noi store de tasklist lay du lieu tu store
import * as actions from '../../actions/index';
class TaskList extends Component {


    constructor(props){
        super(props);
        this.state ={
            filterName: '',
            filterStatus: -1
        };
    }

    onChange = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.value;
        if(name === 'filterStatus'){
            value = parseInt(value, 10);
        }
        this.props.onFilterTask({
            name : name === 'filterName' ? value : this.state.filterName,
            status : name === 'filterStatus' ? value : this.state.filterStatus
        });
        this.setState({
            [name] : value
        });


    }

    render() {
        let {tasks,filterTask, searchName,sort} = this.props;
        if(filterTask){
            if(filterTask.status !== null){
                tasks = tasks.filter((task) => {
                    if(filterTask.status !== -1){
                        let status = task.status === true ? 1 : 0;
                        
                        return status === filterTask.status;
                    }
                    return task;
                });
            }

            if(filterTask.name){
                tasks = tasks.filter((task) => {
                   return task.name.toLowerCase().indexOf(filterTask.name.toLowerCase()) !== -1;
                });
            }

        }
        
        if(searchName !== ''){
            tasks = tasks.filter((task) => {
                return task.name.toLowerCase().indexOf(searchName) !== -1;
             });

        }

        if(sort){
            if(sort.by === 'name'){
                tasks.sort((task01, task02) => {
                    let nameA = task01.name.toLowerCase(),nameB = task02.name.toLowerCase();
                    if(nameA < nameB) return -sort.value; //giam dan
                    if(nameA > nameB) return sort.value; //tang dan
                    return 0;
                });
            }else{
                tasks.sort((task01, task02) => {
                    if(task01.status > task02.status) return -sort.value; //giam dan
                    if(task01.status < task02.status) return sort.value; //tang dan
                    return 0;
                });
            }
        }

        let elmTasks = tasks.map((task, index) => {
            return <TaskListItem 
                key = {index} 
                index = {index} 
                task = {task} 
                />
        });

        let {filterName, filterStatus} = this.state;

        return (
            <div>
                <table className="table table-bordered table-hover">
                    <thead>
                        <tr className="text-center">
                        <th>STT</th>
                        <th>Tên</th>
                        <th>Trạng Thái</th>
                        <th>Hành Động</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td/>
                            <td><div className="form-group">
                                <input 
                                    type="text" 
                                    className="form-control" 
                                    placeholder="Nhập vào tên công việc"
                                    name = 'filterName'
                                    value = {filterName}
                                    onChange = {this.onChange}
                                    />
                                </div></td>
                            <td className="text-center"><div className="form-group">
                                <select 
                                    className="form-control"
                                    name = 'filterStatus'
                                    value = {filterStatus}
                                    onChange = {this.onChange}
                                    >
                                    <option value = {-1}>Tất cả</option>
                                    <option value = {1}>Kích hoạt</option>
                                    <option value = {0}>Ẩn</option>
                                </select>
                                </div></td>
                            <td />
                        </tr>
                        {elmTasks}
                    </tbody>
                </table>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        tasks: state.tasks,
        filterTask: state.filterTask,
        searchName: state.searchTask,
        sort: state.sortTask
    }
};


const mapDispatchToProps = (dispatch,props) => {
    return {
        onFilterTask : (filter) => {
            dispatch(actions.filter_task(filter));
        }
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(TaskList);