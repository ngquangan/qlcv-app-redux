import React, { Component } from 'react';
import * as actions from '../../actions/index';
import {connect} from 'react-redux';
class TaskListItem extends Component {


    onUpdateStatus = () => {
        this.props.onToggleStatus(this.props.task.id);
    }

    onDeleteTask = () => {
        this.props.onCloseForm();
        this.props.onDeleteTask(this.props.task.id);
    }

    onEditTask = () => {
        this.props.onOpenForm();
        this.props.onEditTask(this.props.task)
    }


    render() {
        let {task} = this.props;
        let {index} = this.props;
        return (
            <tr>
                <td>{index + 1}</td>
                <td>{task.name}</td>
                <td className="text-center">
                    <button 
                        className={(task.status) ? " btn btn-success btn-sm" : " btn btn-danger btn-sm" } 
                        onClick = {this.onUpdateStatus}
                    >
                        {(task.status) ? "Kích hoạt" : "Ẩn" }
                    </button>  
                </td>
                <td className="text-center">
                    <button 
                        className="mr-1 btn btn-warning"
                        onClick = {this.onEditTask}
                    >Sửa
                    </button>
                    <button 
                        className="btn btn-danger"
                        onClick = {this.onDeleteTask}
                    >Xóa
                    </button>
                </td>
            </tr>
        );
    }
}

const mapStateToProps = (state) => {
    return {

    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        onOpenForm: () => {
            dispatch(actions.open_form())
        },
        onCloseForm: () => {
            dispatch(actions.close_form())
        },
        onToggleStatus: (id) => {
            dispatch(actions.toggle_status(id));
        },
        onDeleteTask: (id) => {
            dispatch(actions.delete_task(id));
        },
        onEditTask: (task) => {
            dispatch(actions.edit_task(task));
        }
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(TaskListItem);